﻿using UnityEngine;
using System.Collections;

public class GiveHp : MonoBehaviour
{

    [SerializeField] private Collider _targetCollider;
    [SerializeField] private GameObject _toDestroy;
    [SerializeField] private AudioSource _audio;

    void OnTriggerEnter(Collider other_)
    {
        if (other_ == _targetCollider)
        {
            _targetCollider.SendMessage("OnDamageTake", -1);
            _audio.Play();

            Destroy(_toDestroy);
        }
    }
}
