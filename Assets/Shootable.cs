﻿using UnityEngine;
using System.Collections;

public class Shootable : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private AudioSource _audio;

    void OnParticleCollision(GameObject go_)
    {
        var system = go_.GetComponent<ParticleSystem>();
        var arr = new ParticleSystem.CollisionEvent[system.safeCollisionEventSize];
        system.GetCollisionEvents(gameObject, arr);
        foreach (var collisionEvent in arr)
        {
            _rigidbody.AddForceAtPosition(-collisionEvent.normal*1000, collisionEvent.intersection);
        }
        if(_audio != null)
            _audio.Play();

        SendMessage("OnHit", SendMessageOptions.DontRequireReceiver);
    }
}
