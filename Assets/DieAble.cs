﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class DieAble : MonoBehaviour
{
    public event Action OnDamageTaken;

    [SerializeField] private float _startHp;
    [SerializeField] private AudioSource _audio;
    [SerializeField] private ParticleSystem _dieFx;
    [SerializeField] private int _score;
    [SerializeField] private GameObject _activateOnDeath;

    private float _hp;
    private MeshRenderer[] _renderers;

    public float Hp
    {
        get { return _hp; }
    }

    // Use this for initialization
	void Awake ()
	{
	    _renderers = GetComponentsInChildren<MeshRenderer>();
	    _hp = _startHp;
        _dieFx.Stop();
	}

    void OnDamageTake(float damage_)
    {
        if(!enabled) return;
        

        _hp -= damage_;
        
        if(OnDamageTaken != null)
            OnDamageTaken();

        if(damage_ > 0)
            _audio.Play();
        
        if (_hp <= 0f)
        {
            foreach (var meshRenderer in _renderers)
            {
                meshRenderer.enabled = false;
            }
            GetComponent<Collider>().enabled = false;
            

            
            _dieFx.Play();
            float duration = Mathf.Max(_audio.clip.length, _dieFx.duration);
            
            if (_activateOnDeath != null)
                StartCoroutine(ActiovateAfterWait(duration - 0.1f));

            Destroy(gameObject, duration);

            if(_score > 0)
                ScoreManager.I.AddScore(_score);
            
        }

        
    }

    private IEnumerator ActiovateAfterWait(float duration_)
    {
        yield return new WaitForSeconds(duration_);
        _activateOnDeath.SetActive(true);
    }
}
