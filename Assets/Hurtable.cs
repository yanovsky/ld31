﻿using UnityEngine;
using System.Collections;

public class Hurtable : MonoBehaviour
{
    [SerializeField] private AudioSource _audio;
    private Rigidbody _rigidbody;
    private Transform _transform;

    void Awake()
    {
        _rigidbody = rigidbody;
        _transform = transform;
    }

    void OnHurt(Vector3 position_)
    {
        _rigidbody.AddForce(1000*(_transform.position-position_).normalized);
        _audio.Play();
    }
}
