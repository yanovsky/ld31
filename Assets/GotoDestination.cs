﻿using UnityEngine;
using System.Collections;

public class GotoDestination : KindaState
{
    [SerializeField] private Transform _target;
    [SerializeField] private float _updateTime = 1f;

    private NavMeshAgent _navMeshAgent;

	// Use this for initialization
	void Awake ()
	{
	    _navMeshAgent = GetComponent<NavMeshAgent>();
	}

    void OnEnable()
    {
        StartCoroutine(GotoTarget());
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator GotoTarget()
    {
        while (_target != null)
        {
            _navMeshAgent.SetDestination(_target.position);
            yield return new WaitForSeconds(_updateTime);
        }
        
    }
}
