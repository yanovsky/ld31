﻿using UnityEngine;
using System.Collections;

public class HurtPlayer : MonoBehaviour {
    private Transform _transform;

    void Awake()
    {
        _transform = transform;
    }
    void OnTriggerEnter(Collider other_)
    {
        other_.SendMessage("OnHurt", _transform.position, SendMessageOptions.DontRequireReceiver);
    }
}
