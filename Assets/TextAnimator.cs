﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class TextAnimator : MonoBehaviour
{

    [SerializeField] private Text _label;
    private int _value = 0;
    void Update()
    {
        if (_label == null)
            _label = GetComponent<Text>();
    }

    public void Clear()
    {
        _value = 0;
    }

    public void SetNumeric(int value_, string suffix_ = "", float duration_ = 0.5f)
    {
        StopAllCoroutines();
        StartCoroutine(AnimateNumeric(value_, suffix_, duration_));
    }

    public void SetForced(string text_)
    {
        _label.text = text_;
    }

    private IEnumerator AnimateNumeric(int value_, string suffix_, float duration_)
    {
        var interval = 0.05f;
        var delta = value_ - _value;
        var step = delta / (duration_ / interval);

        float middleValue = _value;
        while (middleValue < value_)
        {
            middleValue += step;
            _label.text = suffix_ + Mathf.FloorToInt(middleValue);
            yield return new WaitForSeconds(interval);
        }
        _value = value_;
        _label.text = suffix_ + _value;
    }
}
