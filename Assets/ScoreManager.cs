﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager I;

    [SerializeField] private TextAnimator _scoreText;
    [SerializeField] private Text _comboText;
    [SerializeField] private TextAnimator _currentScoreText;

    private List<int> _currentRow = new List<int>();
    private int _total = 0;

    public int Score
    {
        get
        {
            return _total + _currentRow.Sum();
        }
    }


    void Awake()
    {
        I = this;
        ShowValues();
    }



    private void ShowValues()
    {
        _scoreText.SetForced(_total.ToString());
        _currentScoreText.SetForced("");
        _comboText.text = "";
    }

    public void AddScore(int value_)
    {
        _currentRow.Add(value_);
        StopAllCoroutines();
        StartCoroutine(CheckCombo());
    }

    private IEnumerator CheckCombo()
    {
        if (_currentRow.Count > 1)
            _comboText.text = _currentRow.Count + " COMBO";
        else
            _comboText.text = "";

        var value = 0;
        var adder = 0;
        foreach (var i in _currentRow)
        {
            value += i + adder;
            adder += i;
        }
        _currentScoreText.SetNumeric(value, "+");

        yield return new WaitForSeconds(2f);

        _total += value;
        
        _currentRow.Clear();
        _currentScoreText.Clear();

        _scoreText.SetNumeric(_total);
        _currentScoreText.SetForced("");
        _comboText.text = "";
    }
}
