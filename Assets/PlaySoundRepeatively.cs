﻿using UnityEngine;
using System.Collections;

public class PlaySoundRepeatively : MonoBehaviour {
    private AudioSource _audio;
    private ParticleSystem _particleSystem;

    // Use this for initialization
	void Start ()
	{
	    _audio = audio;
	    _particleSystem = GetComponent<ParticleSystem>();
	    StartCoroutine(PlaySound());
	}

    private IEnumerator PlaySound()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f / _particleSystem.emissionRate);
            _audio.Play();
        }
        
    }
}
