﻿using UnityEngine;
using System.Collections;

public class DeadlyArea : MonoBehaviour {
    [SerializeField] private float _enterDamage;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider collider_)
    {
        collider_.SendMessage("OnDamageTake", _enterDamage, SendMessageOptions.DontRequireReceiver);
    }
}
