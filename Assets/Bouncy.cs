﻿using System;
using UnityEngine;
using System.Collections;

public class Bouncy : MonoBehaviour
{
    private RectTransform _transform;
	// Use this for initialization
	void Start ()
	{
	    _transform = transform as RectTransform;
	    StartCoroutine(Bouncing());
	}

    private IEnumerator Bouncing()
    {
        var t = 0f;
        var size = _transform.sizeDelta;
        while (true)
        {
            _transform.sizeDelta = (1f + Mathf.Sin(t)*0.2f)*size;
            t += 0.1f;
            yield return new WaitForEndOfFrame();
        }
        
    }
}
