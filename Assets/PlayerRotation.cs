﻿using System;
using UnityEngine;
using System.Collections;

public class PlayerRotation : MonoBehaviour
{
    private Camera _camera;
    private int _layerMask;
    private Rigidbody _rigidbody;
	// Use this for initialization
	void Start ()
	{
	    _camera = Camera.main;
	    _layerMask = LayerMask.GetMask("MouseMove");
        _rigidbody = rigidbody;
	}
	
	// Update is called once per frame
	void Update () {
        var ray = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
	   
        if (Physics.Raycast(ray, out hitInfo, Single.PositiveInfinity, _layerMask))
        {
            var direction = hitInfo.point - _rigidbody.position;

            rigidbody.rotation = Quaternion.LookRotation(direction, Vector3.up);
           
        }
	}
}
