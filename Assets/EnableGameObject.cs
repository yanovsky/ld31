﻿using UnityEngine;
using System.Collections;

public class EnableGameObject : KindaState
{
    [SerializeField] private GameObject _toEnable;
    

	// Use this for initialization
    void OnEnable()
    {
        Debug.LogWarning("Enable gun");
	    _toEnable.SetActive(true);
        Finish();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
