﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;



public class EnemyGenerator : MonoBehaviour
{

    class Wave
    {
        public Dictionary<GameObject, int> enemies;
        public string preText;
        public string afterText;
        public string text;
        public float delay;
        public Transform point;
        public string id;
        public bool test;

    }

    [SerializeField] private Transform _pointsParent;
    [SerializeField] private GameObject _enemyStoned;
    [SerializeField] private GameObject _gunBonus;
    [SerializeField] private GameObject _gunBonus2;
    [SerializeField] private GameObject _gunBonus3;
    [SerializeField] private GameObject _hpBonus;
    [SerializeField] private GameObject _enemy1;
    [SerializeField] private GameObject _enemy2;
    [SerializeField] private GameObject _enemyFast;
    [SerializeField] private GameObject _enemyBully;
    [SerializeField] private GameObject _enemyWithGun;
    

    [SerializeField] private Transform _player;
    [SerializeField] private string _startWave;
    [SerializeField] private TextTyper _textTyper;
    [SerializeField] private Transform _point1;
    [SerializeField] private PrepareForTutorCommand _prepareCommand;
    
    private List<Wave> _waves;
    private List<Transform> _points;
    private Dictionary<GameObject, float> _weights;

    public static int WAVE_NUMBER = 0;

    // Use this for initialization
	void Start ()
	{
	    WAVE_NUMBER = 0;
	    if (string.IsNullOrEmpty(_startWave) && PlayerPrefs.HasKey("tutor_complete"))
	    {
	        _startWave = "start";
	        WAVE_NUMBER = 14;
	    }

	    _points = new List<Transform>();
	    for (int i = 0; i < _pointsParent.childCount; i++)
	        _points.Add(_pointsParent.GetChild(i));

	    _waves = new List<Wave>()
	    {
            new Wave(){ preText = "Ave, cubiator!\nI mean gladiator... \nWelcome to cube arena.", delay = 2f},
            new Wave(){ preText = "You can move by WASD\n or by arrow keys", delay = 2f },
            new Wave(){ preText = "Be aware of \nthose spiky borders", delay = 2f },
            new Wave()
            {
                preText = "Take that small greenbox...\nit might be helpful",
	            enemies = new Dictionary<GameObject, int>()
	            {
	                {_gunBonus, 1}
	            },
                point = _point1
            },
            new Wave(){ preText = "Awesome! Now you can\n shoot by holding\n mouse button!", delay = 4f},
            new Wave(){ preText = "Your goal will be...\nkilling bad guys", delay = 2f},
            new Wave(){ preText = "The only way to kill\n anybody on the arena is\nto push them on the spikes", delay = 2f},
            new Wave()
            {
                text = "Try to push that \nred bastard on spikes!\nuse your gun",
	            enemies = new Dictionary<GameObject, int>()
	            {
	                {_enemyStoned, 1}
	            },
                point = _point1
            },
            new Wave()
            {
                text = "Great \nnow some more...",
	            enemies = new Dictionary<GameObject, int>()
	            {
	                {_enemyStoned, 4}
	            }
            },
            new Wave(){ preText = "OK, you're doing well", delay = 1f},
            new Wave()
	        {
                preText = "Now bad guys \nare able to move",
	            enemies = new Dictionary<GameObject, int>()
	            {
	                {_enemy1, 6}
	            }
	        },
            new Wave(){ preText = "Good luck, cubiator\nI mean gladiator...", delay = 1f},
            new Wave(){ preText = ""},
            new Wave()
	        {
                id = "start",
	            enemies = new Dictionary<GameObject, int>()
	            {
	                {_enemy1, 6}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
	                {_enemy1, 7}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemy2, 1}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
	                {_enemy1, 8},
                    {_enemy2, 1}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
	                {_enemy1, 12},
                    {_enemy2, 3}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
	                {_enemy1, 20},
                    {_enemy2, 3}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemyFast, 3}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemyFast, 3},
                    {_enemy1, 6},
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_hpBonus, 1}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemyFast, 6},
                    {_enemy2, 3},
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_gunBonus2, 1}
	            }
	        },
            new Wave()
	        {
                
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemyBully, 1}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemyBully, 4}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_hpBonus, 1}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemyBully, 4},
                    {_enemyFast, 6},
                    {_enemy2, 1}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemyFast, 20}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemy2, 20}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_hpBonus, 2}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemy1, 10}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemy1, 5}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemy1, 10}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemy1, 5}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemyBully, 5}
	            }
	        },
            new Wave()
	        {
                id = "test1",
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_gunBonus2, 1}
	            },
                test = true
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemyBully, 10}
	            }
	        },
            new Wave()
	        {
                id = "test2",
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_gunBonus2, 1}
	            },
                test = true
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemyWithGun, 3},
                    {_enemy1, 10}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_enemyWithGun, 10},
                    {_enemy1, 3}
	            }
	        },
            new Wave()
	        {
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_hpBonus, 3}
	            }
	        },
            new Wave()
	        {
                id = "test3",
	            enemies = new Dictionary<GameObject, int>()
	            {
                    {_gunBonus3, 1}
	            }
	        }
	    };

        _weights = new Dictionary<GameObject, float>()
        {
            {_enemy2, 0.5f},
            {_enemyBully, 1f},
            {_enemyFast, 0.5f},
            {_enemyWithGun, 1f}
        };

	    if (!string.IsNullOrEmpty(_startWave))
	    {
	        foreach (var wave in _waves)
	        {
	            if (wave.id == _startWave)
	            {
	                _waves.RemoveRange(0, _waves.IndexOf(wave));
                    break;
	            }
	        }
	    }
	    else
	    {
	        _prepareCommand.Do();
	    }

	    StartCoroutine(Spawning(1));
	}

    private IEnumerator Spawning(int difficulty_)
    {
        foreach (var wave in _waves)
        {
            WAVE_NUMBER++;
            GA.API.Design.NewEvent("wave:" + WAVE_NUMBER, ScoreManager.I.Score);

#if !UNITY_EDITOR
            if(wave.test)
                continue;
#endif
            if (wave.preText != null)
            {
                _textTyper.SetText(wave.preText);
                while (!_textTyper.AllTextShowed)
                    yield return new WaitForSeconds(0.5f);
            }

            if (wave.text != null)
            {
                _textTyper.SetText(wave.text);
            }
            if (wave.enemies != null)
            {
                if (wave.id == "start")
                    PlayerPrefs.SetInt("tutor_complete", 1);

                var enemiesCount = wave.enemies.Sum(kvp_ => kvp_.Value);
                var points = ChooseRandomPoints(enemiesCount);
                var currentEnemies = new List<Object>();
                foreach (var kvp in wave.enemies)
                {
                    for (int i = 0; i < kvp.Value; i++)
                    {
                        if (points.Count > 0)
                        {
                            var point = points[0];
                            points.RemoveAt(0);

                            if (wave.point != null)
                                point = wave.point;

                            var enemy = Instantiate(kvp.Key, point.position, Quaternion.identity);
                            currentEnemies.Add(enemy);
                        }
                        
                    }
                }

                foreach (var currentEnemy in currentEnemies)
                {
                    while (currentEnemy != null)
                    {
                        yield return new WaitForEndOfFrame();
                    }
                } 
            }

            if (wave.afterText != null)
            {
                _textTyper.SetText(wave.afterText);
                while (!_textTyper.AllTextShowed)
                    yield return new WaitForSeconds(0.5f);
            }

            if (wave.delay > 0f)
                yield return new WaitForSeconds(wave.delay);
        }
        _waves.Clear();

        var curdifficulty = 0f;
        var wav = new Wave();
        wav.enemies = new Dictionary<GameObject, int>();
        var possibleEnemies = _weights.Keys.ToList();
        while (curdifficulty <= difficulty_)
        {
            var choosen = possibleEnemies[Random.Range(0, possibleEnemies.Count - 1)];
            if (wav.enemies.ContainsKey(choosen))
                wav.enemies[choosen]++;
            else
                wav.enemies[choosen] = 1;
            curdifficulty += _weights[choosen];
        }
        _waves.Add(wav);

        yield return StartCoroutine(Spawning(difficulty_ + 1));
    }

    private List<Transform> ChooseRandomPoints(int count_)
    {
        var points = _points.GetRange(0, _points.Count);
        points.Shuffle();

        var retval = new List<Transform>();
        while (retval.Count < count_)
        {
            if (points.Count == 0)
                return retval;
            var pnt = points[0];
            points.RemoveAt(0);

            if (Vector3.Distance(pnt.position, _player.position) > 3f)
            {
                retval.Add(pnt);
            }
        }
        return retval;

    }


}