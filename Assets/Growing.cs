﻿using UnityEngine;
using System.Collections;

public class KindaState : MonoBehaviour
{
    [SerializeField]
    private MonoBehaviour _nextState;

    protected void Finish()
    {
        if (_nextState != null)
        {
            enabled = false;
            _nextState.enabled = true;
        }
    }
}

public class Growing : KindaState
{
    [SerializeField] private int _steps;
    [SerializeField] private float _duration;
    [SerializeField] private float _stepDurationFrames;
    

    private Transform _transform;
    void Awake()
    {
        _transform = transform;
        
    }

    void OnEnable()
    {
        _transform.localScale = Vector3.zero;
        StartCoroutine(MakeGrowing());
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator MakeGrowing()
    {
        for (var currentStep = 0; currentStep < _steps; currentStep++)
        {
            var currentScale = _transform.localScale;
            var needScale = currentScale + Vector3.one/_steps;
            for (var t = 0f; t < 1f; t += 1f / _stepDurationFrames)
            {
                _transform.localScale = Vector3.Lerp(currentScale, needScale, t);
                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForSeconds(_duration / _steps - _stepDurationFrames * Time.fixedDeltaTime);
        }
        Finish();
    }
}
