﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour
{

    [SerializeField] private ParticleSystem _gun;
    private Rigidbody _rigidbody;
    private Transform _transform;
    private AudioSource _audio;
    [SerializeField] private float _delay;
    [SerializeField] private float _kickBack = 1f;

	// Use this for initialization
	void Start ()
	{
	    _gun.enableEmission = false;
	    _transform = transform;
	    _rigidbody = rigidbody;
	    _audio = _gun.audio;
	}

    void OnEnable()
    {
        _gun.gameObject.SetActive(true);
    }
	
	// Update is called once per frame
	void Update ()
	{
	    if (Input.GetButtonDown("Fire1"))
	    {
	        StartCoroutine(Shooting());
	    }
	    else if (Input.GetButtonUp("Fire1"))
	    {
	        StopAllCoroutines();
	    }
	}

    private IEnumerator Shooting()
    {
        while (true)
        {
            _gun.Emit(1);
            _audio.Play();
            yield return new WaitForSeconds(_delay);
        }
        
    }

    public void ChangeGun(ParticleSystem newGun_, float shootDelay_)
    {
        _gun.gameObject.SetActive(false);
        newGun_.gameObject.SetActive(true);
        _delay = shootDelay_;
        _audio = newGun_.GetComponent<AudioSource>();
        newGun_.enableEmission = _gun.enableEmission;
        _gun = newGun_;
        
    }
}
