﻿using UnityEngine;
using System.Collections;

public class EnableComponent : MonoBehaviour
{

    [SerializeField] private Collider _targetCollider;
    [SerializeField] private MonoBehaviour _targetComponent;
    [SerializeField] private GameObject _toDestroy;
    [SerializeField] private AudioSource _audio;

    void OnTriggerEnter(Collider other_)
    {
        if (other_ == _targetCollider)
        {
            _targetComponent.enabled = true;
            _audio.Play();

            Destroy(_toDestroy);
        }
    }
}
