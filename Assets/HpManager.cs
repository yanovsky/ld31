﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HpManager : MonoBehaviour
{
    [SerializeField] private DieAble _target;
    [SerializeField] private Image _image;
    private RectTransform _transform;
	// Use this for initialization
	void Start ()
	{
	    _image = GetComponent<Image>();
	    _target.OnDamageTaken += TargetOnOnDamageTaken;
	    _transform = transform as RectTransform;
	    _transform.sizeDelta = new Vector2(_target.Hp*100f, _transform.sizeDelta.y);
	}

    private void TargetOnOnDamageTaken()
    {
        StopAllCoroutines();
        StartCoroutine(Animate());
    }

    private IEnumerator Animate()
    {
        var oldWidth = _transform.sizeDelta.x;
        var step = 1f;
        var curHp = Mathf.FloorToInt(oldWidth/100f);
        var t = 0f;
        while (Math.Abs(_transform.sizeDelta.x - _target.Hp * 100f) > step)
        {
            _transform.sizeDelta = new Vector2(Mathf.Lerp(_transform.sizeDelta.x, _target.Hp*100f, 0.02f), _transform.sizeDelta.y);
            t += 0.3f;
            if(_target.Hp < curHp)
                _image.color = new Color(1f, 1f, 1f, Mathf.Sin(t)/2 + 0.5f);
            yield return new WaitForEndOfFrame();
        }
        _image.color = new Color(1f, 1f, 1f, 1f);
    }

    // Update is called once per frame
	void Update () {
	
	}
}
