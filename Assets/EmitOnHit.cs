﻿using UnityEngine;
using System.Collections;

public class EmitOnHit : MonoBehaviour
{
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private int _count;
    [SerializeField] private AudioSource _audio;

    void OnHit()
    {
        _particleSystem.transform.position = transform.position;
        _particleSystem.Emit(_count);
        _audio.Play();
    }

}
