﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class PrepareForTutorCommand : MonoBehaviour
{
    [SerializeField] private List<MonoBehaviour> toDisableComp; 
    [SerializeField] private List<GameObject> toDisableGo;


    public void Do()
    {
        Debug.Log("do");
        foreach (var monoBehaviour in toDisableComp)
        {
            monoBehaviour.enabled = false;
        }
        foreach (var o in toDisableGo)
        {
            o.SetActive(false);
        }
    }
}
