﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _maxSpeed;
    [SerializeField] private float _forceMultiplier;
    [SerializeField] private Quaternion _cameraRotor;

    private Rigidbody _rigidbody;

	// Use this for initialization
	void Start ()
	{
	    _rigidbody = rigidbody;
	    _cameraRotor = Quaternion.Euler(0, Camera.main.transform.rotation.eulerAngles.y, 0);
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
	    var move = _forceMultiplier * (_cameraRotor * new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical")));
        _rigidbody.AddForce(move);

	    _rigidbody.velocity = Vector3.ClampMagnitude(_rigidbody.velocity, _maxSpeed);
	}
}
