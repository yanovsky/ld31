﻿using UnityEngine;
using System.Collections;

public class PlayOnEmit : MonoBehaviour
{
    private ParticleSystem _particleSystem;
    private AudioSource _audioSource;
    private int _count = 0;

	// Use this for initialization
	void Start ()
	{
	    _particleSystem = GetComponent<ParticleSystem>();
	    _audioSource = audio;
	}
	
	// Update is called once per frame
	void Update () {
	    if (_particleSystem.particleCount != _count)
	    {
            if(_particleSystem.particleCount > _count)
	            _audioSource.Play();
	        _count = _particleSystem.particleCount;
	    }
	}
}
