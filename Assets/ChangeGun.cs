﻿using UnityEngine;
using System.Collections;

public class ChangeGun : MonoBehaviour
{

    [SerializeField] private Collider _targetCollider;
    [SerializeField] private PlayerShooting _targetComponent;
    [SerializeField] private GameObject _toDestroy;
    [SerializeField] private AudioSource _audio;
    [SerializeField] private ParticleSystem _gun;
    [SerializeField] private float _gunDelay = 0.1f;

    void OnTriggerEnter(Collider other_)
    {
        if (other_ == _targetCollider)
        {
            _targetComponent.ChangeGun(_gun, _gunDelay);
            _audio.Play();

            Destroy(_toDestroy);
        }
    }
}
