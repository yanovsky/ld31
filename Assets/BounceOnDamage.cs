﻿using UnityEngine;
using System.Collections;

public class BounceOnDamage : MonoBehaviour
{
    private Rigidbody _rigidbody;
	// Use this for initialization
	void Start ()
	{
	    _rigidbody = rigidbody;
	}

    void OnDamageTake(float damage_)
    {
        if(damage_ > 0)
            _rigidbody.AddForce(-rigidbody.velocity.normalized*7000f);
    }
}
