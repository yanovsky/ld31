﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextTyper : MonoBehaviour {

    [SerializeField]
    private Text _text;
    [SerializeField]
    private float _charDuration;

    public bool AllTextShowed { get; private set; }

    public void SetText(string text_)
    {
        StopAllCoroutines();
        if (text_ == "")
            _text.text = "";
        else
            StartCoroutine(ShowText(text_));
    }

    private IEnumerator ShowText(string text_)
    {
        AllTextShowed = false;
        var chars = text_.ToCharArray();
        var toShow = "";
        for (var i = 0; i < chars.Length; i++)
        {
            toShow += chars[i];
            _text.text = toShow;
            yield return new WaitForSeconds(_charDuration);
        }
        AllTextShowed = true;

    }
}
