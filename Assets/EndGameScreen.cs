﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndGameScreen : MonoBehaviour
{
    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _scoreLabel;
    [SerializeField] private Text _highScoreText;
    [SerializeField] private GameObject _highScoreGo;
	// Use this for initialization
	void Start () {
	
	}

    void OnEnable()
    {

        var score = ScoreManager.I.Score;
        
        _scoreText.text = score.ToString();

        var highScore = 0;
        if (PlayerPrefs.HasKey("highscore"))
        {
            highScore = PlayerPrefs.GetInt("highscore");
        }

        if (score > highScore)
        {
            highScore = score;
            _scoreLabel.text = "New highscore!";
            _highScoreGo.SetActive(false);
        }
        else
        {
            _scoreLabel.text = "Score";
            _highScoreGo.SetActive(true);
        }

        _highScoreText.text = highScore.ToString();

        PlayerPrefs.SetInt("highscore", highScore);
        GA.API.Design.NewEvent("die:" + EnemyGenerator.WAVE_NUMBER, score); 
    }

    public void Restart()
    {
        Application.LoadLevel(0);
    }
}
